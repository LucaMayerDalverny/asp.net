using System.Diagnostics.CodeAnalysis;
using Isen.Dotnet.Library.Model;
using Microsoft.EntityFrameworkCore;

namespace Isen.Dotnet.Library.Context
{    
    public class ApplicationDbContext : DbContext
    {        
        // Listes des classes modèle / tables
        public DbSet<Person> Persons { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Role> Roles {get; set;}
        public DbSet<PersonRole> PersonRoles {get; set;}

        public ApplicationDbContext(
            [NotNullAttribute] DbContextOptions options) : 
            base(options) {  }

        protected override void OnModelCreating(
            ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Tables et relations
            modelBuilder
                // Associer la classe Person...
                .Entity<Person>()
                // ... à la table Person
                .ToTable(nameof(Person))
                // Description de la relation Person.Service
                .HasOne(p => p.Service)
                // Relation réciproque (omise)
                .WithMany()
                // Clé étrangère qui porte cette relation
                .HasForeignKey(p => p.ServiceId);
            // // Pareil pour ResidenceCity
            // modelBuilder.Entity<Person>()
            //     .HasOne(p => p.ResidenceCity)
            //     .WithMany()
            //     .HasForeignKey(p => p.ResidenceCityId);
            // Et utiliser le champ Id comme clé primaire
            // Déclaration optionnelle, car le nommage
            // Id ou PersonId est reconnu comme convention
            // pour les clés primaires ou étrangères
            modelBuilder.Entity<Person>()
                .HasKey(p => p.Id);

            // Pareil pour City
            modelBuilder
                .Entity<Service>()
                .ToTable(nameof(Service))
                .HasKey(s => s.Id);
            
            modelBuilder.Entity<Role>()
                .ToTable(nameof(Role))
                .HasKey(r => r.Id);
            
            modelBuilder.Entity<PersonRole>()
                .ToTable(nameof(PersonRole))
                .HasOne(pr => pr.Person)
                .WithMany()
                .HasForeignKey(pr => pr.PersonId);
            
            modelBuilder.Entity<PersonRole>()
                .HasOne(pr => pr.Role)
                .WithMany()
                .HasForeignKey(pr => pr.RoleId);
        }

    }
}